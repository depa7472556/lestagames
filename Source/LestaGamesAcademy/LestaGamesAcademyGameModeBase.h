// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LestaGamesAcademyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LESTAGAMESACADEMY_API ALestaGamesAcademyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
